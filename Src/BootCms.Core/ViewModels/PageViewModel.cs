﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Boot.Multitenancy;
using BootCms.Models;
using Boot.Multitenancy.Extensions;
using  BootCms.Extensions;
using NHibernate;

namespace BootCms.ViewModels
{
    
    /// <summary>
    /// PageViewModel
    /// Serves views with various methods.
    /// </summary>
    public class PageViewModel
    {
        
        /// <summary>
        /// Get a value from Settings table.
        /// </summary>
        /// <param name="key">The key</param>
        /// <returns>The value</returns>
        public string Settings(string key)
        {
            return SessionFactory
                .For<Settings>()
                  .OpenSession()
                    .Find<Settings>(s => s.Key == key).FirstOrDefault()
                      .Value;
        }


        /// <summary>
        /// A list of all Pages that is Active.
        /// </summary>
        public List<Page> Pages
        {
            get
            {
                return SessionFactory
                    .For<Page>()
                      .OpenSession()
                        .All<Page>();
            }
        }


        /// <summary>
        /// Returns the current Page.
        /// </summary>
        public Page CurrentPage
        {
            get
            {
                return Pages
                    .Find(p => p.Action == string.Empty.Action() && p.Controller == string.Empty.Controller());
            }
        }


        /// <summary>
        /// Fast way to return the "Home" Page.
        /// </summary>
        public Page Home
        {
            get
            {
                return Pages
                    .Find(p => p.Action == string.Empty.HomeAction() && p.Controller == string.Empty.HomeController());
            }
        }
    }
}