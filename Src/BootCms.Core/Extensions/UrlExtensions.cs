﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BootCms.Extensions
{
    public static class UrlExtensions
    {
        public static string Controller(this string s)
        {
            return HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString();
        }

        public static string Action(this string s)
        {
            return HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString();
        }

        public static string HomeController(this string s)
        {
            return "Home";
        }

        public static string HomeAction(this string s)
        {
            return "Index";
        }

        public static string DomainName(this string s)
        {
            return new Uri(HttpContext.Current.Request.Url.ToString())
                .GetComponents(
                UriComponents.AbsoluteUri &
                ~UriComponents.Port &
                ~UriComponents.Path &
                ~UriComponents.Scheme,
                UriFormat.UriEscaped)
                .Replace("www.", string.Empty);
        }

    }
}