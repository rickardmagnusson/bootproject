﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BootCms.ViewModels;

namespace BootCms.Controllers
{
    public abstract class ModuleController<T> : Controller where T : class
    {
        protected dynamic Model { get; set; }

        protected ModuleController()
        {
            Model = (T)Activator.CreateInstance<T>();
        } 
	}
}