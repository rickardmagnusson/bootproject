﻿
using System.Web.Mvc;
using BootCms.ViewModels;

namespace BootCms.Controllers
{
    public abstract class BaseController : Controller
    {
        protected PageViewModel Model { get; set; }

        protected BaseController()
        {
            Model = new PageViewModel();
        }
	}
}