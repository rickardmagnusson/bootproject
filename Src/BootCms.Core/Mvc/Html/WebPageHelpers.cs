﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Boot.Multitenancy;
using Boot.Multitenancy.Extensions;
using BootCms.Models;
using WebGrease.Css.Extensions;

namespace BootCms.Mvc.Html
{
    public static partial class HtmlHelpers
    {
        public static IHtmlString Zone(this HtmlHelper html, Region region)
        {
            return new HtmlString(string.Empty);
        }

        public static MvcHtmlString TopMenu(this HtmlHelper html)
        {
            var sb = new StringBuilder();
            using (var session = SessionFactory.For<Page>().OpenSession())
            {
                (from pages in session.All<Page>()
                    select pages)
                    .Where(a => a.Active).ToList()
                    .ForEach(p =>
                    {
                        var li = new TagBuilder("li");
                        var link = ActionLink(html, p.Title, p.Action, p.Controller, new {});
                        li.InnerHtml = link.ToString();
                        sb.AppendLine(li.ToString());
                    });
            }
            return new MvcHtmlString(sb.ToString());
        }

        public static IHtmlString ActionLink(this HtmlHelper html,string linkText,string actionName,string controllerName,object htmlAttributes)
        {
            var attributes = new RouteValueDictionary(htmlAttributes);
            var linkTag = new TagBuilder("a");
            linkTag.SetInnerText(linkText);
            linkTag.MergeAttributes(attributes);

            var url = new UrlHelper(html.ViewContext.RequestContext);
            linkTag.Attributes.Add("href", url.Action(actionName, controllerName));

            return new HtmlString(linkTag.ToString(TagRenderMode.Normal));
            
        }

        public static bool IsCurrent(string s)
        {
            return false;
        }
    }
}