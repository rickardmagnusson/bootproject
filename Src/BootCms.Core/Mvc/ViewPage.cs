﻿using BootCms.Mvc.Html;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace BootCms.Mvc
{
    /// <summary>
    /// Overrides default ViewPage to inject a new HtmlHelper.
    /// </summary>
    /// <typeparam name="TModel">Current ViewModel</typeparam>
    public abstract class ViewPage<TModel> : WebViewPage<TModel>
    {
        private static ViewContext context;
        private static IViewDataContainer container;
        private readonly HtmlHelperFactory factory = new HtmlHelperFactory();
        public static new HtmlHelper<TModel> Html { get; private set; }

        private void CreateHelper(TModel model)
        {
            Html = factory.CreateHtmlHelper(model, new StringWriter(new StringBuilder()));
        }

        internal virtual TModel BaseModel
        {
            get { return base.Model; }
            set { CreateHelper(value); }
        }

        /// <summary>
        /// Initialize new helpers
        /// </summary>
        public override void InitHelpers()
        {
            base.InitHelpers();
            context = ViewContext;
            container = this;
            Html = new HtmlHelper<TModel>(context, container);
        }



        #region Expose methods for views to HtmlHelpers

        /// <summary>
        /// Reads from Resource.resx and translates the key into selected culture.
        /// </summary>
        /// <param name="key">The key to translate.</param>
        /// <returns>The translated word.</returns>
        public dynamic R(string key)
        {
            return Html.R(key);
        }

        /// <summary>
        /// Zones
        /// </summary>
        /// <param name="region">Placement Region</param>
        /// <returns>A rendered Zone.</returns>
        public dynamic Zone(Region region)
        {
            return Html.Zone(region);
        }

        /// <summary>
        /// TopMenu
        /// </summary>
        /// <returns>A rendered TopMenu.</returns>
        public dynamic TopMenu()
        {
            return Html.TopMenu();
        }

        #endregion


    }
}