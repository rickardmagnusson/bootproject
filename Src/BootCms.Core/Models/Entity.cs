﻿using FluentNHibernate.Mapping;

namespace BootCms.Models
{
    public abstract class Entity<T> : ClassMap<T> where T : class
    {

    }

    public interface IEntity
    {

    }
}