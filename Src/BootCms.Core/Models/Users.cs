﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BootCms.Models
{
    public class User : IEntity
    {
        public virtual Int32 Id { get; set; }
        public virtual string Title { get; set; }
        public virtual Region Region { get; set; }
        public virtual Int32 PageId { get; set; }
        public virtual string Html { get; set; }
        public virtual bool Active { get; set; }
    }

    public class UserMap : Entity<User>
    {
        public UserMap()
        {
            Id(x => x.Id)
               .Column("Id")
               .GeneratedBy.Assigned()
               .CustomType<Int32>();
            Map(p => p.Title);
            Map(p => p.Region);
            Map(p => p.PageId);
            Map(p => p.Html);
            Map(p => p.Active);
        }
    }
}