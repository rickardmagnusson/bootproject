﻿

using System;

namespace BootCms.Models
{
    public class Settings : IEntity
    {
        public virtual Int32 Id { get; set; }
        public virtual string Key { get; set; }
        public virtual string Value { get; set; }      
    }

    public class SettingsMap : Entity<Settings>
    {
        public SettingsMap()
        {
            Id(x => x.Id)
               .Column("Id")
               .GeneratedBy.Assigned()
               .CustomType<Int32>();
            Map(p => p.Key);
            Map(p => p.Value);
        }
    }
}