﻿using System;

namespace BootCms.Models
{
    public abstract class Module : IModule
    {
        public virtual Int32 Id { get; set; }
        public virtual Region Region { get; set; }

        protected Module()
        {
        }
    }

    public interface IModule
    {
        int Id { get; set; }
        Region Region { get; set; }
    }
}