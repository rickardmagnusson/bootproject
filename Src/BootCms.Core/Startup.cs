﻿using BootCms.Environment;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BootCms.Startup))]
namespace BootCms
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Host.Init();
        }
    }
}
