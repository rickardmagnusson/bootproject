﻿
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Boot.Multitenancy;
using Microsoft.Owin;

namespace BootCms.Environment
{
    public static class Host 
    {
        /// <summary>
        /// Initializing tenants and register them into nHibernate.
        /// </summary>
        public static void Init()
        {
            if (!EnvironmentSetup())
            {
                SessionFactoryContainer.Current
                    .Add("localhost",
                        new Tenant(1, "Data Source=|DataDirectory|boot1.sdf;Persist Security Info=False;")
                            .Create())
                    .Add("bootcms.se",
                        new Tenant(2, "Data Source=|DataDirectory|boot2.sdf;Persist Security Info=False;")
                            .Create())
                    .Add("bootcms.com",
                        new Tenant(3, "Data Source=|DataDirectory|boot3.sdf;Persist Security Info=False;")
                            .Create());
            }
            else
            {
                InitSetup();
            }
        }

        public static void InitSetup()
        {
            var routes = System.Web.Routing.RouteTable.Routes;

            if (!EnvironmentSetup())
            {
                routes.Clear();
                routes.MapRoute(
                    name: "Setup",
                    url: "{controller}/{action}/{id}",
                    defaults: new { controller = "Setup", action = "Index", id = UrlParameter.Optional });
            }
            else
            {
                //Restore route in case of none.
                routes.Clear();
                routes.MapRoute(
                    name: "Default",
                    url: "{controller}/{action}/{id}",
                    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional });
            }
        }

        /// <summary>
        /// Check if we need to install a database.
        /// </summary>
        /// <returns></returns>
        private static bool EnvironmentSetup()
        {
            return Directory.EnumerateFiles(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "*.sdf").Any().Equals(false);
        }
    }
}