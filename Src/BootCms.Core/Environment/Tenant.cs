﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.Linq;
using System.Web;
using Boot.Multitenancy;
using BootCms.Filters;
using BootCms.Filters.Extensions;
using BootCms.Models;
using FluentNHibernate.Automapping;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate.Tool.hbm2ddl;

namespace BootCms.Environment
{
    public class Tenant : ISessionFactoryCreator
    {
        public static string Connectionstring { get; set; }
        public static Int32 Id { get; set; }


        /// <summary>
        /// Creates a new instance of Tenant.
        /// </summary>
        /// <param name="tenantId">The tenant id</param>
        /// <param name="connectionstring">The database connectionstring</param>
        public Tenant(Int32 tenantId, string connectionstring)
        {
            Id = tenantId;
            Connectionstring = connectionstring;
        }

        /// <summary>
        /// Builds a Fluent nHibernate ISessionFactory.
        /// </summary>
        /// <returns>ISessionFactory</returns>
        public NHibernate.ISessionFactory Create()
        {
            return Fluently
                  .Configure()
                  .Database(DatabaseConfiguration)
                  .Mappings(MapAssemblies)
                  .ExposeConfiguration(BuildSchema)
                  .ExposeConfiguration(ValidateSchema)
                  .BuildSessionFactory();
        }

        /// <summary>
        ///     Maps all Entities in bin folder.
        /// </summary>
        /// <param name="fmc">MappingConfiguration mapping</param>
        private static void MapAssemblies(MappingConfiguration fmc)
        {
            (from a in AppDomain.CurrentDomain.GetAssemblies()
             select a
                 into assemblies
                 select assemblies)
                .ToList()
                .ForEach(a =>
                {   //Ignore other assemblies since there's no Entity's created in them. (MSCoreLib makes load fail)
                    if (a.FullName.StartsWith("Boot"))
                    {
                        fmc.AutoMappings.Add(AutoMap.Assembly(a)
                            .OverrideAll(p => p.SkipProperty(typeof(NoProperty)))
                            .Where(IsEntity));
                    }
                });
        }

        /// <summary>
        ///     Check when either a Type is a real Entity.
        /// </summary>
        /// <param name="t">Type to check</param>
        /// <returns>True if Entity</returns>
        private static bool IsEntity(Type t)
        {
            return typeof(IEntity).IsAssignableFrom(t);
        }


        /// <summary>
        ///     Set database information.
        /// </summary>
        /// <returns>IPersistenceConfigurer configuration</returns>
        private static IPersistenceConfigurer DatabaseConfiguration()
        {
            return MsSqlCeConfiguration.MsSqlCe40
                .UseOuterJoin()
                .ConnectionString(Connectionstring)
                .ShowSql();
        }


        /// <summary>
        ///     Validates nHibernate schema
        /// </summary>
        /// <param name="config">Configuration config</param>
        private static void ValidateSchema(NHibernate.Cfg.Configuration config)
        {
            var check = new SchemaValidator(config);
        }


        /// <summary>
        ///     Creates or build the current database.
        /// </summary>
        /// <param name="config"></param>
        private static void BuildSchema(NHibernate.Cfg.Configuration config)
        {
            try
            {
                SchemaMetadataUpdater.QuoteTableAndColumns(config);
                new SchemaUpdate(config).Execute(false, true);
            }
            catch //BELOW FOR TEST ONLY!!
            {
                new SqlCeEngine { LocalConnectionString = Connectionstring }.CreateDatabase();

                SchemaMetadataUpdater.QuoteTableAndColumns(config);
                new SchemaUpdate(config).Execute(false, true);
            }
        }
    }
}