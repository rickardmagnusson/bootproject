﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using BootCms.Extensions;

namespace BootCms.Environment
{
    public class TenantConfiguration
    {
        public Int32 GetCurrentTenantId()
        {
            try
            {
                return GetList().Find(d => d.Domain == string.Empty.DomainName())
                    .TenantId;
            }
            catch // On any failure, return default
            {
                return 1;
            }
        }

        public void Add(Int32 tenantId, string connectionstring, string domain, string databasename)
        {
            var data = new Data { TenantId = tenantId, ConnectionString = connectionstring, Domain = string.Empty.DomainName() };

            var writer = new XmlSerializer(typeof(Data));
            using (var file = File.OpenWrite(ContructPath(domain, databasename)))
            {
                writer.Serialize(file, data);
            }
        }

        private string ContructPath(string domain, string databasename)
        {
            var path = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            Directory.CreateDirectory(string.Format(@"{0}\{1}", path, domain));
            return string.Format(@"{0}\{1}\{1}.xml", path, databasename);
        }

        public List<Data> GetList()
        {
            var list = new List<Data>();
            var files = Directory.GetFiles(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "*.xml",
                                         SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var reader = new XmlSerializer(typeof(Data));
                using (var input = File.OpenRead(file))
                {
                    list.Add(reader.Deserialize(input) as Data);
                }
            }

            return list;
        }

        public Data Get(Int32 tenantId)
        {
            var files = Directory.EnumerateFiles(AppDomain.CurrentDomain.GetData("DataDirectory").ToString(), "*.xml", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                var reader = new XmlSerializer(typeof(Data));
                using (var input = File.OpenRead(file))
                {
                    var data = reader.Deserialize(input) as Data;
                    if (data.TenantId == tenantId)
                        return data;
                }
            }
            return null;
        }

        [Serializable()]
        public class Data
        {
            public int TenantId { get; set; }
            public String ConnectionString { get; set; }
            public string Domain { get; set; }
        }

    }
}