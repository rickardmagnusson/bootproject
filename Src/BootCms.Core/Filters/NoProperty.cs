﻿using System;

namespace BootCms.Filters
{
    /// <summary>
    /// Ignores Properties marked with NoProperty.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class NoProperty : Attribute {
    }
}