﻿using System;

namespace BootCms.Filters
{
    /// <summary>
    /// Tell the current Property to not be persisted to table.
    /// Ignores classes marked with NoEntity.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class NoEntity : Attribute
    {
    }
}