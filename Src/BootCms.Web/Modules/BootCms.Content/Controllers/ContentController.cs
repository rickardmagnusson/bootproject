﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BootCms.Controllers;
using BootCms.Modules.Models;

namespace BootCms.Modules.Controllers
{
    public class ContentController : ModuleController<Content>
    {
        public ActionResult Index()
        {
            return View(base.Model);
        }

        public ActionResult Edit()
        {
            return View(base.Model);
        }

        [Authorize]
        [HttpPost()]
        public ActionResult Post(FormCollection form)
        {
            if (ModelState.IsValid)
            {
                
            }

            return View(base.Model);
        }

        [Authorize]
        public ActionResult List()
        {
            return View(base.Model);
        }
    }
}