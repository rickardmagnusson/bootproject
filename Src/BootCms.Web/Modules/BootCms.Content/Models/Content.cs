﻿
using System;
using BootCms.Models;

namespace BootCms.Modules.Models
{
    public class Content : Module
    {
        public virtual Int32 Id { get; set; }
        public virtual string Title { get; set; }
        public virtual Region Region { get; set; }
        public virtual Int32 PageId { get; set; }
        public virtual string Html { get; set; }
        public virtual bool Active { get; set; }
    }

    public class ContentMap : Entity<Content>
    {
        public ContentMap()
        {
            Id(x => x.Id)
               .Column("Id")
               .GeneratedBy.Assigned()
               .CustomType<Int32>();
            Map(p => p.Title);
            Map(p => p.Region);
            Map(p => p.PageId);
            Map(p => p.Html);
            Map(p => p.Active);
        }
    }
}